const app = require('../src/app')

describe('\'customers\' service', () => {
  it('registered the service', () => {
    const service = app.service('customers')
    expect(service).toBeTruthy()
  })
  it('Get All Customer List', () => {
    const service = app.service('customers')
    expect(service.find()).toBeTruthy()
  })
})

describe('\'orders\' service', () => {
  it('registered the service', () => {
    const service = app.service('orders')
    expect(service).toBeTruthy()
  })
  it('Get All Order List', () => {
    const service = app.service('orders')
    expect(service.find()).toBeTruthy()
  })
})
