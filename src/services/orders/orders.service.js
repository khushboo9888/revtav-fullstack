// Initializes the `orders` service on path `/orders`
const { Orders } = require('./orders.class')
const createModel = require('../../models/orders.model')
const hooks = require('./orders.hooks')

module.exports = async function (app) {
  const options = {
    Model: createModel(app)
  }

  // Initialize our service with any options it requires
  app.use('/orders', new Orders(options, app))

  // Initialize our custom route
  app.get('/analytics', async (req, res) => {
    const data = await options.Model.aggregate([
      {
        $group:
        {
          _id: { day: { $dayOfMonth: '$createdAt' } },
          orderCount: { $sum: 1 },
          totalPrice: { $sum: '$price' }
        }
      },
      {
        $replaceRoot: {
          newRoot: {
            day: '$_id.day',
            orderCount: '$orderCount',
            totalPrice: '$totalPrice'
          }
        }
      },
      {
        $sort: {
          day: 1
        }
      }
    ])
    res.send(data)
  })

  // Get our initialized service so that we can register hooks
  const service = app.service('orders')

  service.hooks(hooks)
}
