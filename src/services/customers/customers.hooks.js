var mongoose = require('mongoose')
const { authenticate } = require('@feathersjs/authentication').hooks

const {
  hashPassword, protect
} = require('@feathersjs/authentication-local').hooks

module.exports = {
  before: {
    all: [],
    find: [
      // authenticate('jwt')
    ],
    get: [
      // authenticate('jwt')
    ],
    create: [hashPassword('password')],
    update: [hashPassword('password'), authenticate('jwt')],
    patch: [hashPassword('password'), authenticate('jwt')],
    remove: [authenticate('jwt')]
  },

  after: {
    all: [
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      protect('password')
    ],
    find: [async context => {
      const values = await Promise.all(context.result.map(customer => {
        return context.app.service('orders').find({ query: { customer: mongoose.Types.ObjectId(customer._id) } })
      }))
      values.map((value, index) => {
        delete context.result[index].createdAt
        delete context.result[index].updatedAt
        context.result[index].orders = value
      })
      return context
    }],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
}
