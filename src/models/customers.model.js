module.exports = function (app) {
  const modelName = 'customers'
  const mongooseClient = app.get('mongooseClient')
  const schema = new mongooseClient.Schema({
    firstName: { type: String },
    lastName: { type: String },
    email: { type: String, unique: true, lowercase: true },
    password: { type: String },
    created: { type: Date, default: new Date() }
  }, {
    timestamps: true
  })

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(modelName)) {
    mongooseClient.deleteModel(modelName)
  }
  return mongooseClient.model(modelName, schema)
}
